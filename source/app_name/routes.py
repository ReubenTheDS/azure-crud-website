from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from app_name import app


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Change %r>' % self.id


@app.route('/', methods=['POST', 'GET'])
@app.route("/home", methods=['POST', 'GET'])
def home():
    if request.method == 'POST':
        change_content = request.form['content']
        new_change = Todo(content=change_content)

        try:
            db.session.add(new_change)
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue adding your change'

    else:
        changes = Todo.query.order_by(Todo.date_created).all()
        return render_template('home.html', changes=changes)


@app.route('/delete/<int:id>')
def delete(id):
    change_to_delete = Todo.query.get_or_404(id)

    try:
        db.session.delete(change_to_delete)
        db.session.commit()
        return redirect('/')
    except:
        return 'There was a problem deleting that change'


@app.route('/update/<int:id>', methods=['GET', 'POST'])
def update(id):
    change = Todo.query.get_or_404(id)

    if request.method == 'POST':
        change.content = request.form['content']

        try:
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue updating your change'

    else:
        return render_template('update.html', change=change)
