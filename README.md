# Push Python Flask-Waitress image to Azure web-app

## Deployment Flow
This CI CD pipeline can be used to push to an Azure web-app that has been provisioned using the Azure CLI/Portal


## Prerequisites
* An Azure account
* [An Azure Resource Group](https://docs.microsoft.com/en-us/cli/azure/group?view=azure-cli-latest)
* [An existing web-app to deploy to](https://docs.microsoft.com/en-us/cli/azure/webapp?view=azure-cli-latest)
* [An Azure Container Registry](https://docs.microsoft.com/en-us/cli/azure/acr?view=azure-cli-latest)
* [A Web hook that updates the web-app on pushing to the Azure Container Registry](https://docs.microsoft.com/en-us/cli/azure/acr/webhook?view=azure-cli-latest)
